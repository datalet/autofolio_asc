'''
Created on Dec 8, 2014

@author: Marius Lindauer
'''

import os
import sys
import json
import re
import csv
import logging

class SMAC(object):
    '''
        interface to the algorithm configurator SMAC
    '''


    def __init__(self, root_path="", reps=1, budget=1, cutoff=1, wrapper=None, tmp_path=None, runsolver=None):
        '''
        Constructor
        '''
        self.reps = reps
        self.budget = budget
        self.cutoff = cutoff
        self.wrapper = wrapper
        self.tmp_path = tmp_path

        self.root_path = root_path        
        self.runsolver = runsolver
        self._PATH = os.path.join(root_path,"configurators/smac-v2.08.00-master-731/smac")
        
        self._TRAJ_THRESHOLD = sys.maxint
        
    def get_ac_calls(self, pcss, inner_splits, outdir):
        '''
            1. writes configuration scenarios 
            2. builds calling strings
        '''
        
        with open(os.path.join(outdir,"train.txt"), "w") as fp:
            for i in xrange(inner_splits):
                fp.write("%d\n" %(i+1))
        with open(os.path.join(outdir,"test.txt"), "w") as fp:
            for i in xrange(inner_splits):
                fp.write("%d\n" %(i+1))
                
        with open(os.path.join(outdir,"config_wrapper.json"), "w") as fp:
            json.dump({"runsolver": self.runsolver,
                       "tmp": self.tmp_path
                       }, fp)
        
        cmds = []
        for pcs in pcss:
            base_pcs = os.path.basename(pcs)
            base_pcs = base_pcs.strip(".pcs")
            with open(os.path.join(outdir,base_pcs+".scen"), "w") as fp:
                fp.write("execdir = .\n")
                fp.write("deterministic = 1\n")
                # fp.write("run_obj = quality\n")
                fp.write("run_obj = runtime\n")
                fp.write("overall_obj = mean\n")
                fp.write("cutoff_time = %d\n" %(self.cutoff))
                fp.write("wallclock-limit = %d\n" %(self.budget))
                fp.write("paramfile = %s\n" %(pcs))
                fp.write("algo = python %s %s\n" %(self.wrapper, os.path.join(outdir,"config_wrapper.json")))
                fp.write("test_instance_file = %s\n" %(os.path.join(outdir,"test.txt")))
                fp.write("instance_file = %s\n" %(os.path.join(outdir,"train.txt")))
                
            for seed in xrange(self.reps):
                log_file = os.path.join(outdir,"%s_%d.log" %(base_pcs, seed+1))
                cmd = "%s/configurators/smac-v2.08.00-master-731/smac --scenarioFile %s --seed %d --always-run-initial-config True --validation False --output-dir %s > %s 2>&1" %(
                            self.root_path, os.path.join(outdir,base_pcs+".scen"), seed+1, 
                            os.path.join(outdir, "smac-output"), log_file)
                cmds.append(cmd)
                
        return cmds
            
        
    def get_best_conf(self, scen, out_dir):
        '''
           looks for a given scenario_name into smac-output; 
           parses validationCallStrings-traj-run-\d+-walltime.csv and validationResults-traj-run-\d+-walltime.csv
        '''
        regex_traj = re.compile("log-run(?P<rep>\d+).txt")
        
        rep_conf = {}
        
        out_dir_ = os.path.join(out_dir, "smac-output", scen)
        for file_ in os.listdir(out_dir_):
            full_file_ = os.path.join(out_dir_, file_)
            if not os.path.isfile(full_file_):
                continue
            
            match_traj = regex_traj.match(file_)
            
            if match_traj:
                rep = int(match_traj.group("rep"))
                rep_conf[rep] = rep_conf.get(rep,{})
                with open(full_file_) as fp:
                    next_ = False
                    for line in fp:
                        line = line.replace("\n","")
                        if line.startswith("Sample call for this final incumbent"):
                            next_ = True
                            continue
                        if next_:
                            print(line)
                            first_param = line.find("@") #super ugly, but it is the only way to get only active parameters
                            rep_conf[rep]["conf"] = line[first_param-1 :]
                            rep_conf[rep]["perf"] = 0
                            break
                    #===========================================================
                    # parts = res_line.split(",")
                    # rep_conf[rep]["perf"] = float(parts[1])
                    # conf = map(lambda x: "-"+x.strip(" "), parts[5:])
                    # conf_splited = []
                    # for c in conf:
                    #     conf_splited.extend(c.split("="))
                    # rep_conf[rep]["conf"] = " ".join(conf_splited)
                    #===========================================================
        

        sorted_reps = sorted(rep_conf.items(), key=lambda x: x[1]["perf"])
        
        return sorted_reps[0][1]["conf"], sorted_reps[0][1]["perf"]    
        
    def get_traj_perfs(self, scen, id_):
        '''
            looks for a given scenario in smac-output
            and returns the trajectories ([]) for each smac run (time, perf)
        '''
        regex_traj = re.compile("traj-run-(?P<rep>\d+).txt")
        rep_traj = []
        
        out_dir_ = os.path.join("smac-output", scen)
        
        scen_file = os.path.join(out_dir_, "state-run%d" %(id_), "scenario.txt")
        with open(scen_file) as fp:
            for line in fp:
                if line.startswith("wallclock-limit"):
                    self.budget = float(line.split("=")[1]) 
        
        for file_ in os.listdir(out_dir_):
            full_file_ = os.path.join(out_dir_, file_)
            if not os.path.isfile(full_file_):
                continue
            match_traj = regex_traj.match(file_)
            if match_traj:
                with open(full_file_) as fp:
                    csv_reader = csv.reader(fp, delimiter=",", quotechar='"')
                    incumbent_id = -1
                    last_perf = None
                    trajectory = []
                    for row in csv_reader:
                        try:
                            time = float(row[0])
                            performance = float(row[1])
                            incumbent_curr = int(row[3])
                        except ValueError:
                            continue
                        if performance > self._TRAJ_THRESHOLD :
                            continue
                        
                        # first real point will be saved but ignored for the moment
                        if not last_perf:
                            last_perf = performance
                            incumbent_id = incumbent_curr
                            continue
                        
                        if incumbent_id != incumbent_curr:
                            if not trajectory: #save first point
                                trajectory.append((1, last_perf))
                            trajectory.append((time,last_perf))
                        last_perf = performance
                        incumbent_id = incumbent_curr
                    if not trajectory: # if there was only one line in the trajectory file
                        trajectory.append((1, last_perf)) 
                    trajectory.append((self.budget , performance))
                    rep_traj.append(trajectory) 
        return rep_traj