#! /usr/bin/env python
'''

comments
========

Author: hearntest
'''

import os
import sys
from subprocess import Popen

#=======================================================
#=======================================================
#=======================================================



def main(args):

  scenarios =[
    "Caren",
    "Mira",
    "Magnus",
    "Monty",
    "Quill",
    "Bado",
    "Svea",
    "Sora",
    'MAXSAT19-UCMS',
    'SAT18-EXP',
    'GLUHACK-2018'
  ]


  for scen in scenarios:

    # path = "test_out/"+scen
    # if not os.path.exists(path):
    #   os.makedirs(path)

    cmd = 'python test.py -s autofolio_arff/test/' + scen +' -m trained/'+scen+ ' -o test_out'
    print(cmd)
    proc = Popen(cmd.split())
    proc.communicate()




if __name__ == '__main__':
  main(sys.argv[1:])
  