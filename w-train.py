#! /usr/bin/env python
'''

comments
========

Author: hearntest
'''

import os
import sys
from subprocess import Popen

#=======================================================
#=======================================================
#=======================================================



def main(args):

  scenarios =[
    "Caren",
    "Mira",
    "Magnus",
    "Monty",
    "Quill",
    "Bado",
    "Svea",
    "Sora"
  ]

  scenarios +=[
    'MAXSAT19-UCMS',
    'SAT18-EXP',
    'GLUHACK-2018'
  ]

  # scenarios = ['Bado']

  if len(args) == 0:
    sys.exit('[scenarioname]')


  scen = args[0]

  path = "trained/"+scen
  if not os.path.exists(path):
    os.makedirs(path)

  cmd = 'python train.py -s autofolio_arff/train/' + scen + ' -o ' + path
  print(cmd)
  proc = Popen(cmd.split())
  proc.communicate()




if __name__ == '__main__':
  main(sys.argv[1:])
  