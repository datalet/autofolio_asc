# builds a python virtual environment with all required packages for flexfolio

virtualenv .
pip install -I numpy==1.9.2 
pip install -I scipy==0.15.1
pip install -I matplotlib==1.4.3 
pip install -U liac-arff 
pip install -I scikit-learn==0.15.0
