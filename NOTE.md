# notes


## FIRST AND FOREMOST !!!

. test/bin/activate


## Installation - configure virtualenv

- . bin/active before execution

## install JAVA


## Other remarks

python ./train.py -s example/CSP-2010-train -o train_out

time limit
see train.py line
145
169


## commands
python ./train.py -s example/CSP-2010-train -o train_out

python ./test.py -s example/CSP-2010-test -m train_out -o test_out

python ./train.py -s autofolio_arff/train/Monty -o train_out


python ./test.py -s autofolio_arff/test/Monty -m train_out -o test_out

### Training

`cd <root-folder>`  
`mkdir <training-output-directory>`  
`python ./train.py -s <ASlib-scenario> -o <training-output-directory -p <presolver> -t <presolver_runtime>`


### Testing



`cd <root-folder>`  
`mkdir <testing-output-directory>`  
`python ./test.py -s <ASlib-scenario> -m <training-output-directory> -o <testing-output-directory> -p <presolver> -t <presolver-runtime>`


## Changes

smac.py quality to runtime


## For more,
https://bitbucket.org/mlindauer/autofolio_asc/src/master/


### pragmatics



python w-train.py 


  scenarios =[
    "Caren",
    "Mira",
    "Magnus",
    "Monty",
    "Quill",
    "Bado",
    "Svea",
    "Sora"
    'MAXSAT19-UCMS',
    'SAT18-EXP',
    'GLUHACK-2018'
  ]

 When done, do

 cd af/autofolio_asc/;. test/bin/activate; screen

 cd af/autofolio_asc/;. test/bin/activate

python train.py -s autofolio_arff/train/Magnus -o trained/Magnus
python train.py -s autofolio_arff/train/Bado -o trained/Bado
python train.py -s autofolio_arff/train/Quill -o trained/Quill
python train.py -s autofolio_arff/train/Sora -o trained/Sora
python train.py -s autofolio_arff/train/MAXSAT19-UCMS -o trained/MAXSAT19-UCMS


python ./test.py -s autofolio_arff/test/Monty -m trained/Monty -o test_out
python ./test.py -s autofolio_arff/test/Mira -m trained/Mira -o test_out



python ./test.py -s autofolio_arff/test/Sora -m trained/Sora -o test_out
python ./test.py -s autofolio_arff/test/Magnus -m trained/Magnus -o test_out
python ./test.py -s autofolio_arff/test/Bado -m trained/Bado -o test_out
python ./test.py -s autofolio_arff/test/MAXSAT19-UCMS -m trained/MAXSAT19-UCMS -o test_out
python ./test.py -s autofolio_arff/test/Quill -m trained/Quill -o test_out


# --------- 19/05/2021 ---------


python train.py -s autofolio_arff/train/Magnus -o train_out/Magnus
python train.py -s autofolio_arff/train/Bado -o train_out/Bado
python train.py -s autofolio_arff/train/Quill -o train_out/Quill
python train.py -s autofolio_arff/train/Sora -o train_out/Sora
python train.py -s autofolio_arff/train/MAXSAT19-UCMS -o train_out/MAXSAT19-UCMS

python ./test.py -s autofolio_arff/test/Magnus -m train_out/Magnus -o test_out
python ./test.py -s autofolio_arff/test/Bado -m train_out/Bado -o test_out
python ./test.py -s autofolio_arff/test/Quill -m train_out/Quill -o test_out
python ./test.py -s autofolio_arff/test/Sora -m train_out/Sora -o test_out
python ./test.py -s autofolio_arff/test/MAXSAT19-UCMS -m train_out/MAXSAT19-UCMS -o test_out


python train.py -s autofolio_arff_fs/train/Magnus -o train_out_fs/Magnus





